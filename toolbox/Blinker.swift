//
//  Blinker.swift
//  Wordbuzz
//
//  Created by verec on 05/03/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation

class Blinker : NSObject {

    struct State {
        var index: Int = 0
        var cycles = [NSTimeInterval]()
        var toggle:Bool = false

        mutating func next() -> NSTimeInterval {
            let c = cycles[index]
            index += 1
            if index >= cycles.count {
                index = 0
            }
            toggle = !toggle
            return c
        }
    }

    var state: State {
        get {
            return _state
        }
        set {
            _state = newValue

            if _state.cycles.count > 0 {
                _state.index = 0
                self.start()
            } else {
                self.stop()
            }

        }
    }

    var _state = State()

    var blinker: ((Bool)->())?

    var timer:NSTimer! = nil
}

extension Blinker {

    private func start() {
        stop()
        let t = _state.next()
        self.timer = NSTimer.scheduledTimerWithTimeInterval(
                            t
            ,   target:     self
            ,   selector:   #selector(Blinker.tick(_:))
            ,   userInfo:   nil
            ,   repeats:    false)

    }

    private func stop() {
        if self.timer != nil {
            self.timer.invalidate()
            self.timer = nil
        }
    }

    @objc func tick(timer: NSTimer) {
        self.blinker?(state.toggle)
        start()
    }
}