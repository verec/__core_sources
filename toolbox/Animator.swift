//
//  Animator.swift
//  WordbuzzLite
//
//  Created by verec on 25/04/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import QuartzCore
import Foundation

class Animator {

    var displayLink:    CADisplayLink!  = nil
    var tick:           Int             = 0

    var nextFrame:  ((Int)->Bool)?  = .None

    var running : Bool {
        return displayLink != nil
    }
}

extension Animator {

    func stop() {
        if self.displayLink != nil {
            self.displayLink.invalidate()
            self.displayLink = nil
        }
    }

    func start() {
        stop()
        self.displayLink = CADisplayLink(target: self, selector: #selector(Animator.display(_:)))
        self.displayLink.addToRunLoop(NSRunLoop.mainRunLoop(), forMode: NSRunLoopCommonModes)
    }

    @objc func display(link: CADisplayLink) {
        if let stop = nextFrame?(self.tick) {
            self.tick += 1
            if stop {
                self.stop()
            }
        }
    }
}