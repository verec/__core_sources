//
//  Pattern.swift
//  WordbuzzLite
//
//  Created by verec on 25/04/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import CoreGraphics

import Foundation
import UIKit

struct Pattern {

    var phase: CGSize   =   CGSize.zero
    var color: UIColor  =   UIColor.blueColor()

    func suspendPhase() {
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        CGContextSetPatternPhase(context, CGSize.zero)
    }

    func applyFillColor() {
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        CGContextSetPatternPhase(context, self.phase)
        CGContextSetFillColorWithColor(context, color.CGColor)
    }

    func applyStrokeColor() {
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        CGContextSetPatternPhase(context, self.phase)
        CGContextSetStrokeColorWithColor(context, color.CGColor)
    }

    func applyStrokeFillColor() {
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        CGContextSetPatternPhase(context, self.phase)
        CGContextSetStrokeColorWithColor(context, color.CGColor)
        CGContextSetFillColorWithColor(context, color.CGColor)
    }
}