//
//  UIDevice.swift
//  __core_sources
//
//  Created by verec on 30/12/2015.
//  Copyright © 2015 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UIDevice {

    class func isLandscape() -> Bool {
        let device = UIDevice.currentDevice()
        if !device.generatesDeviceOrientationNotifications {
            device.beginGeneratingDeviceOrientationNotifications()
        }
        return UIDeviceOrientationIsLandscape(device.orientation)
    }
}