//
//  UIEdgeInsets.swift
//  WordbuzzLite
//
//  Created by verec on 07/05/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UIEdgeInsets {

    func top(top:CGFloat) -> UIEdgeInsets {
        var insets = self
        insets.top = top
        return insets
    }

    func left(left:CGFloat) -> UIEdgeInsets {
        var insets = self
        insets.left = left
        return insets
    }

    func bottom(bottom:CGFloat) -> UIEdgeInsets {
        var insets = self
        insets.bottom = bottom
        return insets
    }

    func right(right:CGFloat) -> UIEdgeInsets {
        var insets = self
        insets.right = right
        return insets
    }
}