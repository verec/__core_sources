//
//  UITextView.swift
//  Wordbuzz
//
//  Created by verec on 21/02/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UITextView : UIScrollViewDelegate {

    public func scrollViewWillEndDragging(scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {

        if let font = self.font {
            let lineHeight  = font.lineHeight

            targetContentOffset.memory = snapToIntegerRowForProposedContentOffset(targetContentOffset.memory, rowHeight: lineHeight)
        }
    }
}