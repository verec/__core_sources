//
//  CGAffineTransform.swift
//  Wordbuzz
//
//  Created by verec on 08/05/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import CoreGraphics
import Foundation

extension CGAffineTransform {

    func translate(tx: CGFloat, _ ty: CGFloat) -> CGAffineTransform {
        return CGAffineTransformTranslate(self, tx, ty)
    }

    func scale(sx: CGFloat, _ sy: CGFloat) -> CGAffineTransform {
        return CGAffineTransformScale(self, sx, sy)
    }

    func scale(scale: CGFloat) -> CGAffineTransform {
        return CGAffineTransformScale(self, scale, scale)
    }

    func rotate(angle: CGFloat) -> CGAffineTransform {
        return CGAffineTransformRotate(self, angle)
    }
}
