//
//  UIFont.swift
//  Wordbuzz
//
//  Created by verec on 28/02/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {

    func deriveFontWithSymbolicTraits(traits: [UIFontDescriptorSymbolicTraits]) -> UIFont {
        let nt = UIFontDescriptorSymbolicTraits(rawValue: traits
            .reduce(0) { $0 | $1.rawValue}
        )
        let fd = self.fontDescriptor().fontDescriptorWithSymbolicTraits(nt)
        return UIFont(descriptor: fd, size: self.pointSize)
    }
}