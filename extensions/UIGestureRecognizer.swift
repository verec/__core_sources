//
//  UIGestureRecognizer_Extension.swift
//  Wordbuzz
//
//  Created by verec on 30/05/2015.
//  Copyright (c) 2015 Cantabilabs Ltd. All rights reserved.
//

import Darwin

import Foundation
import UIKit

extension UIGestureRecognizer {

    func touchToStandardTrigonometricAngleFromCenter(center: CGPoint? = .None) -> Math.Radian? {

        if let view = self.view {

            /// Note that we will swap point.y & origin.y to compensate for the
            /// flipped coordinate system.

            let origin      = center ?? CGPoint(x: view.bounds.midX, y: view.bounds.midY)
            let point       = self.locationInView(view)
            let adj_oppo    = CGPoint(x: point.x - origin.x, y: origin.y - point.y)
            let hypothenuse = CGFloat(sqrt(adj_oppo.x * adj_oppo.x + adj_oppo.y * adj_oppo.y))

            if hypothenuse > 0.0 {
                let cos     = acos(adj_oppo.x / hypothenuse)
                let sin     = asin(adj_oppo.y / hypothenuse)

                return sin < 0.0 ? Math.Constants.πx2 - cos : cos
            }
        }

        return .None
    }

    /// Returns the angle between the touch location and the supplied center
    /// point in standard trigonometric 2D space: ie angle ranges from 0.0 to π
    /// when moving from 3 o'clock to 9 o'clock, anti clockwise. If left
    /// unspecified, the center defaults to the actual center of the view this
    /// recognizer has been added to.

    func angle(center: CGPoint? = .None) -> Math.Radian? {
        return touchToStandardTrigonometricAngleFromCenter(center)
    }

    /// Returns the angle between the touch location and the supplied center
    /// point in clock coordinate with 0.0 corresponging to 12 noon and π
    /// corresponding to 6 o'clock. If left unspecified the center defaults to
    /// the actual center of the view this recognizer has been added to.

    func clockWise(center: CGPoint? = .None) -> Math.Radian? {
        if let angle = angle(center) {
            return Math.Constants.πx2 - fmod(angle + Math.Constants.πx3_2, Math.Constants.πx2)
        }
        return .None
    }
}

extension UIGestureRecognizerState {

    var description: String {
        switch self {
            case    .Possible:  return "Possible"
            case    .Began:     return "Began"
            case    .Changed:   return "Changed"
            case    .Ended:     return "Ended"
            case    .Cancelled: return "Cancelled"
            case    .Failed:    return "Failed"
        }
    }
}