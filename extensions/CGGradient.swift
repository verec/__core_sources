//
//  CGGradient.swift
//  Wordbuzz Lite
//
//  Created by verec on 16/04/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import CoreGraphics
import UIKit

extension CGGradient {

    static func gradient(colors: [UIColor]) -> CGGradient? {
        assert(colors.count > 1)
        var running = CGFloat(0)
        let increment:CGFloat = 1.0 / CGFloat(colors.count - 1)
        let locations:[CGFloat] = colors.map {
            let _ = $0
            let r = running
            running += increment
            return r
        }
        return gradient(colors, locations: locations)
    }

    static func gradient(colors: [UIColor], locations:[CGFloat]) -> CGGradient? {
        let colorSpace  = CGColorSpaceCreateDeviceRGB()
        let cgColors    = colors.map { $0.CGColor }
        return CGGradientCreateWithColors(colorSpace, cgColors, locations)
    }

    func colorForSize(size: CGSize) -> UIColor {
        let options =
            CGGradientDrawingOptions(rawValue:
                CGGradientDrawingOptions.DrawsBeforeStartLocation.rawValue
            |   CGGradientDrawingOptions.DrawsAfterEndLocation.rawValue)

        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.mainScreen().scale)
        let context = UIGraphicsGetCurrentContext()
        let startPoint = CGPoint(x:0.0, y: 0.0)
        let endPoint = CGPoint(x:size.width, y: 0.0)
        CGContextDrawLinearGradient(context, self, startPoint, endPoint, options)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return UIColor(patternImage: image)
    }
}