//
//  Set.swift
//  WordbuzzLite
//
//  Created by verec on 28/04/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Swift
import Foundation

extension Set {
    subscript(e:Element) -> Bool {
        return self.contains(e)
    }
}

