//
//  NSAttributeString.swift
//  Sahara
//
//  Created by verec on 30/03/2016.
//  Copyright © 2016 GFM. All rights reserved.
//

import Foundation
import UIKit

extension NSAttributedString {

    class func seed() -> NSAttributedString {
        return NSAttributedString()
    }

    func append(text:String, font:UIFont, color:UIColor, bgColor:UIColor? = .None) -> NSAttributedString {
        
        let m:NSMutableAttributedString = NSMutableAttributedString(attributedString: self)

        let n:NSMutableAttributedString = NSMutableAttributedString(string: text)
        let r = NSRange(location: 0, length: n.length)
        n.addAttribute(NSFontAttributeName, value: font, range: r)
        n.addAttribute(NSForegroundColorAttributeName, value: color, range: r)
        if let bgColor = bgColor {
            n.addAttribute(NSBackgroundColorAttributeName, value: bgColor, range:r)
        }

        m.appendAttributedString(n)
        return m
    }

    func withLineSpacing(lineSpacing: CGFloat) -> NSAttributedString {
        let m:NSMutableAttributedString = NSMutableAttributedString(attributedString: self)
        let r = NSRange(location: 0, length: m.length)
        let p = NSMutableParagraphStyle()
        p.lineHeightMultiple = lineSpacing
        m.addAttribute(NSParagraphStyleAttributeName, value: p, range: r)
        return m
    }

    func decreaseFontSize(decreaseBy: CGFloat = 1.0) -> NSAttributedString {
        let text = self

        let new = NSMutableAttributedString(string: text.string)

        let range = NSRange(location: 0, length: text.length)

        text.enumerateAttributesInRange(range
        , options: NSAttributedStringEnumerationOptions(rawValue: 0)) {
            (d: [String : AnyObject], r: NSRange, stop: UnsafeMutablePointer<ObjCBool>) in

            var newD = [String:AnyObject]()
            for key in d.keys {
                if key == NSFontAttributeName {
                    if let value = d[key] as? UIFont {
                        let size = value.pointSize - decreaseBy
                        if size > 6.0 {
                            let newFont = value.fontWithSize(size)
                            newD[key] = newFont
                            continue
                        }
                    }
                }
                if let value = d[key] {
                    newD[key] = value
                }
            }

            new.addAttributes(newD, range: r)
        }
        
        return new
    }
}