//
//  ScreenSizeModel.swift
//  Wordbuzz
//
//  Created by verec on 20/09/2015.
//  Copyright © 2015 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

var ScreenSizeModel_Counter = 0

struct ScreenSizeModel {
    let iPad:       Bool
    let iPhone:     Bool
    let pointSize:  CGSize
    let uniqueId:   Int

    init(iPad:Bool = false, pointSize:CGSize) {
        self.iPad = iPad
        self.iPhone = !iPad
        self.pointSize = pointSize
        ScreenSizeModel_Counter += 1
        self.uniqueId = ScreenSizeModel_Counter
    }

    /// http://www.paintcodeapp.com/news/ultimate-guide-to-iphone-resolutions
    /// http://www.iphoneresolution.com
    static let size0    =   CGSize(width: 320.0, height: 480.0)
    static let size1    =   CGSize(width: 320.0, height: 568.0)
    static let size2    =   CGSize(width: 375.0, height: 667.0)
    static let size3    =   CGSize(width: 414.0, height: 736.0)

    static let size10   =   CGSize(width: 768.0, height: 1024.0)
    static let size11   =   CGSize(width: 1024.0, height: 1366.0)

    static let screenSize_3_5in     =   ScreenSizeModel(pointSize: size0)
    static let screenSize_4_0in     =   ScreenSizeModel(pointSize: size1)
    static let screenSize_4_7in     =   ScreenSizeModel(pointSize: size2)
    static let screenSize_5_5in     =   ScreenSizeModel(pointSize: size3)

    static let iPad             =   ScreenSizeModel(iPad: true, pointSize: size10)
    static let iPad_Pro         =   ScreenSizeModel(iPad: true, pointSize: size11)

    static let knownModels = [
        screenSize_3_5in
    ,   screenSize_4_0in
    ,   screenSize_4_7in
    ,   screenSize_5_5in
    ,   iPad
    ,   iPad_Pro
    ]

    static let currentModel: ScreenSizeModel? = {

        func inferModel() -> ScreenSizeModel? {
            let size = UIScreen.mainScreen().bounds.size

            let d = UIDevice.currentDevice()
            if !d.generatesDeviceOrientationNotifications {
                debug("### starting device orientation events generation")
                d.beginGeneratingDeviceOrientationNotifications()
            }

            let models  =   d.userInterfaceIdiom == .Phone
                        ?   [   ScreenSizeModel.screenSize_3_5in
                            ,   ScreenSizeModel.screenSize_4_0in
                            ,   ScreenSizeModel.screenSize_4_7in
                            ,   ScreenSizeModel.screenSize_5_5in]
                        :   [   ScreenSizeModel.iPad
                            ,   ScreenSizeModel.iPad_Pro]


            for hwm in models {

                if hwm.pointSize == size {
                    return hwm
                }
            }
            return .None
        }
        return inferModel()
        }()
}

func == (lhs: ScreenSizeModel, rhs: ScreenSizeModel) -> Bool {
    return  lhs.pointSize == rhs.pointSize
        &&  lhs.iPad == rhs.iPad
        &&  lhs.iPhone == rhs.iPhone
        &&  lhs.uniqueId == rhs.uniqueId
}

extension ScreenSizeModel : Hashable {
    var hashValue: Int {
        return Int(self.pointSize.width * self.pointSize.height) + self.uniqueId
    }
}