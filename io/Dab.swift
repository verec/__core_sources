//
//  Dab.swift
//  Wordbuzz
//
//  Created by verec on 07/02/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

/// dab
/// noun: dab; plural noun: dabs
/// 1.
/// a small amount of something.
/// "she licked a dab of chocolate from her finger"
/// synonyms:	drop, dash, spot, smear, dribble, trickle, splash, sprinkle,
/// speck, taste, lick, trace, touch, hint, suggestion, soupçon, particle,
/// bit, modicum

import Foundation

class Dab {
    let token:  String
    var attributes = [String]()
    var children = [Dab]()

    init() {
        self.token = ""
    }

    init(token:String) {
        self.token = token
    }
}

extension Dab {

    subscript(path: String) -> Dab? {
        let comps = path.NS.componentsSeparatedByString("/")
        var node = self
        for comp in comps {
            let coulda = node.children.filter {
                return $0.token == comp
            }
            if coulda.count == 1 {
                return coulda.first
            }
            if coulda.count > 0 {
                node = coulda.first!
            }
        }
        return .None
    }
}

extension Dab {
    
    func each(fn:Dab->()) {
        let _ = self.children.filter {
            fn($0)
            return false
        }
    }
}

extension Dab {

    func encode(dab: Dab) -> NSData? {
        return .None
    }

    class func decode(data: NSData) -> Dab? {
        return .None
    }
}
