//
//  UIView+FixUIViewDebugging.h
//  WordbuzzLite
//
//  Created by verec on 06/05/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (FixUIViewDebugging)

@end
