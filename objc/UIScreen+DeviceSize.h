//
//  UIScreen+DeviceSize.h
//  UEFA
//
//  Created by verec on 25/02/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

@import Foundation ;
@import UIKit ;


typedef NS_ENUM(NSInteger, DeviceSize) {

    DeviceSize_Unknown  = -1

,   DeviceSize_3_5in    = 0
,   DeviceSize_4_0in
,   DeviceSize_4_7in
,   DeviceSize_5_5in
,   DeviceSize_iPad
,   DeviceSize_iPadPro

,   DeviceSize_Count

} NS_ENUM_AVAILABLE(10_8, 6_0) ;

@interface UIScreen (DeviceSize)

- (DeviceSize) deviceSize ;

@end
