//
//  NSObject+Assoc.m
//  __core_sources
//
//  Created by verec on 30/10/2014.
//  Copyright (c) 2014 Cantabilabs Ltd. All rights reserved.
//

#import <objc/runtime.h>

#import "NSObject+Assoc.h"

@implementation NSObject (Assoc)

- (NSMutableDictionary *) assoc {

    NSMutableDictionary * dict = objc_getAssociatedObject(self, @selector(assoc)) ;
    if (dict == nil) {
        dict = [@{} mutableCopy] ;
        objc_setAssociatedObject(self, @selector(assoc), dict, OBJC_ASSOCIATION_RETAIN) ;
    }
    return dict ;
}

@end
