//
//  NSString+Bonjour.m
//  __core_sources
//
//  Created by verec on 06/11/2014.
//  Copyright (c) 2014 Cantabilabs Ltd. All rights reserved.
//

#import "NSString+Bonjour.h"

@implementation NSString (Bonjour)

- (NSString *) bonjourLocalName {
    NSMutableString * out = [NSMutableString new] ;
    NSString * source = [self lowercaseString] ;

    for (NSUInteger index = 0, count = [source length] ; index < count ; ++index) {
        unichar c = [source characterAtIndex:index] ;
        if (c == ' ') {
            [out appendString:@"-"] ;
        } else if (c >= 'a' && c <= 'z') {
            [out appendFormat:@"%C", c] ;
        } else if (c >= '0' && c <= '9') {
            [out appendFormat:@"%C", c] ;
        }
    }
    [out appendString:@".local"] ;
    return [NSString stringWithString:out] ;
}

@end
