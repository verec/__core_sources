//
//  CustomFonts.h
//  __core_sources
//
//  Created by verec on 02/06/2013.
//  Copyright (c) 2014 Cantabilabs Ltd. All rights reserved.
//

@import Foundation ;
@import UIKit.UIFont ;

@interface CustomFonts : NSObject

- (UIFont *) customFontWithName: (NSString *) name
                          style: (NSString *) style
                           size: (CGFloat) size ;

+ (instancetype) customFonts ;

- (UIFont *) fontWithName: (NSString *) name blackOfSize: (CGFloat) size ;
- (UIFont *) fontWithName: (NSString *) name blackItalicOfSize: (CGFloat) size ;
- (UIFont *) fontWithName: (NSString *) name boldOfSize: (CGFloat) size ;
- (UIFont *) fontWithName: (NSString *) name boldItalicOfSize: (CGFloat) size ;
- (UIFont *) fontWithName: (NSString *) name hairlineOfSize: (CGFloat) size ;
- (UIFont *) fontWithName: (NSString *) name hairlineItalicOfSize: (CGFloat) size ;
- (UIFont *) fontWithName: (NSString *) name lightOfSize: (CGFloat) size ;
- (UIFont *) fontWithName: (NSString *) name lightItalicOfSize: (CGFloat) size ;
- (UIFont *) fontWithName: (NSString *) name italicOfSize: (CGFloat) size ;
- (UIFont *) fontWithName: (NSString *) name regularOfSize: (CGFloat) size ;

@end
