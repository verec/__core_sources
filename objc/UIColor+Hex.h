//
//  UIColor+Hex.h
//  __core_sources
//
//
//  Created by verec on 10/07/2013.
//  Copyright (c) 2013 Inspired Now Ltd. All rights reserved.
//

@import UIKit ;

@interface UIColor (Hex)

+ (UIColor *) rgbColor: (UInt32) rgb ;
+ (UIColor *) rgbaColor: (UInt32) rgba ;
+ (UIColor *) rgbaColorFromString: (NSString *) rgbaString ;

+ (UIColor *) oldlaceColor ;
+ (UIColor *) lightcyanColor ;
+ (UIColor *) lavenderColor ;
+ (UIColor *) lightsteelblueColor ;
+ (UIColor *) paleturquoiseColor ;
+ (UIColor *) powderblueColor ;


+ (UIColor *)   red255: (CGFloat) red255
              green255: (CGFloat) green255
               blue255: (CGFloat) blue255
              alpha255: (CGFloat) alpha255 ;

- (CGFloat *) rgbValues: (CGFloat *) outValues ;

- (UIColor *) colorWithHue: (CGFloat) unitHue ;
- (UIColor *) colorWithSaturation: (CGFloat) unitSaturation ;
- (UIColor *) colorWithBrightness: (CGFloat) unitBrightness ;

+ (UIColor *) whiteIsh ;
+ (UIColor *) blueIsh ;
+ (UIColor *) blueerIsh ;

@end

// 
