//
//  UIView+FixUIViewDebugging.m
//  WordbuzzLite
//
//  Created by verec on 06/05/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

#ifdef DEBUG

#import <UIKit/UIKit.h>
#import <objc/runtime.h>

#import "UIView+FixUIViewDebugging.h"

@implementation UIView (FixUIViewDebugging)

+ (void) load {
    Method original = class_getInstanceMethod(self, @selector(viewForBaselineLayout));
    class_addMethod(self, @selector(viewForFirstBaselineLayout), method_getImplementation(original), method_getTypeEncoding(original));
    class_addMethod(self, @selector(viewForLastBaselineLayout), method_getImplementation(original), method_getTypeEncoding(original));
}

@end

#endif
