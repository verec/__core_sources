//
//  NSString+HTMLQueryParameters.m
//  UEFA
//
//  Created by verec on 02/03/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

@import Foundation ;

#import "NSString+HTMLQueryParameters.h"

@implementation NSString (HTMLQueryParameters)

- (NSDictionary<NSString*, NSString*> *) queryDictionary {
    NSMutableDictionary * d = [@{} mutableCopy] ;

    NSArray * params = [self componentsSeparatedByString:@"&"] ;
    for (NSString * param in params) {
        NSArray * kvp = [param componentsSeparatedByString:@"="] ;
        if ([kvp count] > 1) {
            d[kvp[0]] = kvp[1] ;
        }
    }


    return [NSDictionary dictionaryWithDictionary:d] ;
}

- (NSString *) stringByConvertingFromHTMLEncoding {
    NSString * source = self ;
    source = [source stringByReplacingOccurrencesOfString:@"+" withString:@" "] ;
    source = [source stringByRemovingPercentEncoding] ;
    return source ;
}

- (NSString *) stringByStrippingHTMLQuery {
    NSString * source = self ;
    NSArray * parts = [source componentsSeparatedByString:@"?"] ;
    return parts && [parts count] > 0 ? parts[0] : source ;
}




@end
