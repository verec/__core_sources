//
//  NSString+Bonjour.h
//  __core_sources
//
//  Created by verec on 06/11/2014.
//  Copyright (c) 2014 Cantabilabs Ltd. All rights reserved.
//

@import Foundation ;

@interface NSString (Bonjour)

- (NSString *) bonjourLocalName ;

@end
