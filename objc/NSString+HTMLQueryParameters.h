//
//  NSString+HTMLQueryParameters.h
//  UEFA
//
//  Created by verec on 02/03/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

@import Foundation ;

@interface NSString (HTMLQueryParameters)

- (NSDictionary<NSString*, NSString*> *) queryDictionary ;

@property(nonatomic, strong, readonly)  NSString * stringByConvertingFromHTMLEncoding ;
@property(nonatomic, strong, readonly)  NSString * stringByStrippingHTMLQuery ;
@end
