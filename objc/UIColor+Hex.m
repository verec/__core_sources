//
//  UIColor+Hex.m
//  __core_sources
//
//
//  Created by verec on 10/07/2013.
//  Copyright (c) 2013 Inspired Now Ltd. All rights reserved.
//

#include <ctype.h>
#import "UIColor+Hex.h"

@implementation UIColor (Hex)

+ (UIColor *) rgbColor: (UInt32) rgb {
    rgb <<= 8 ;
    rgb |= 0x0ff ;
    return [self rgbaColor:rgb] ;
}

+ (UIColor *) rgbaColor: (UInt32) rgbaColor {
    CGFloat a = rgbaColor & 0x0ff ;
    rgbaColor >>= 8 ;
    CGFloat b = rgbaColor & 0x0ff ;
    rgbaColor >>= 8 ;
    CGFloat g = rgbaColor & 0x0ff ;
    rgbaColor >>= 8 ;
    CGFloat r = rgbaColor & 0x0ff ;

    a /= 255.0f ;
    r /= 255.0f ;
    g /= 255.0f ;
    b /= 255.0f ;

    return [self colorWithRed:r
                        green:g
                         blue:b
                        alpha:a] ;
}

+ (UIColor *) rgbaColorFromString: (NSString *) rgbaString {
    UInt32 rgba = 0 ;
    for (NSUInteger i = 0, count = [rgbaString length] ;  i < count ; ++i) {
        unichar C = [rgbaString characterAtIndex:i] ;
        C = tolower(C) ;
        if (C >= 'a' && C <= 'f') {
            C -= 'a' ;
            C += 10 ;
        } else if (C >= '0' && C <= '9' ) {
            C -= '0' ;
        } else {
            C = 0 ;
        }
        rgba <<= 4 ;
        rgba |= C ;
    }

    return [self rgbaColor:rgba] ;
}

+ (UIColor *) oldlaceColor {
    return [self rgbColor:0xfdf5e6] ;
}

+ (UIColor *) lightcyanColor {
    return [self rgbColor:0xe0ffff] ;
}

+ (UIColor *) lavenderColor {
    return [self rgbColor:0xe6e6fa] ;
}

+ (UIColor *) lightsteelblueColor {
    return [self rgbColor:0xb0c4de] ;
}

+ (UIColor *) powderblueColor {
    return [self rgbColor:0xafeeee] ;
}

+ (UIColor *) paleturquoiseColor {
    return [self rgbColor:0xb0e0e6] ;
}

+ (UIColor *)   red255: (CGFloat) red255
              green255: (CGFloat) green255
               blue255: (CGFloat) blue255
              alpha255: (CGFloat) alpha255 {

    red255 /= 255.0f ;
    green255 /= 255.0f ;
    blue255 /= 255.0f ;
    alpha255 /= 255.0f ;

    return [self colorWithRed: red255
                        green: green255
                         blue: blue255
                        alpha: alpha255] ;
}

- (CGFloat *) rgbValues: (CGFloat *) outValues {
    if (outValues) {
        if (![self getRed: &outValues[0]
                    green: &outValues[1]
                     blue: &outValues[2]
                    alpha: &outValues[3]]) {
            return nil ;
        }
    }
    return outValues ;
}

+ (UIColor *) whiteIsh {
    return [self red255: 204.0f
               green255: 224.0f
                blue255: 244.0f
               alpha255: 255.0f] ;
}

+ (UIColor *) blueIsh {
    return [self red255: 29.0f
               green255: 156.0f
                blue255: 215.0f
               alpha255: 255.0f] ;
}

+ (UIColor *) blueerIsh {
    return [self red255: 0.0f
               green255: 50.0f
                blue255: 126.0f
               alpha255: 255.0f] ;
}

- (UIColor *) colorWithHue: (CGFloat) unitHue {
    CGFloat h = 0.0f ;
    CGFloat s = 0.0f ;
    CGFloat b = 0.0f ;
    CGFloat a = 0.0f ;

    if ([self getHue:&h saturation:&s brightness:&b alpha:&a]) {
        return [UIColor colorWithHue:unitHue saturation:s brightness:b alpha:a] ;
    }
    return self ;
}

- (UIColor *) colorWithSaturation: (CGFloat) unitSaturation {
    CGFloat h = 0.0f ;
    CGFloat s = 0.0f ;
    CGFloat b = 0.0f ;
    CGFloat a = 0.0f ;

    if ([self getHue:&h saturation:&s brightness:&b alpha:&a]) {
        return [UIColor colorWithHue:h saturation:unitSaturation brightness:b alpha:a] ;
    }
    return self ;
}

- (UIColor *) colorWithBrightness: (CGFloat) unitBrightness {
    CGFloat h = 0.0f ;
    CGFloat s = 0.0f ;
    CGFloat b = 0.0f ;
    CGFloat a = 0.0f ;

    if ([self getHue:&h saturation:&s brightness:&b alpha:&a]) {
        return [UIColor colorWithHue:h saturation:s brightness:unitBrightness alpha:a] ;
    }
    return self ;
}



@end
