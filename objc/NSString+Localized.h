//
//  NSString+Localized.h
//  UEFA
//
//  Created by verec on 24/02/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Localized)

- (NSString *) messageLocalized ;
- (NSString *) emptyLocalized ;

@end
