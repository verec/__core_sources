//
//  NSString+PubSub.h
//  __core_sources
//
//
//  Created by verec on 10/06/2013.
//  Copyright (c) 2013 Cantabilabs Ltd. All rights reserved.
//

@import Foundation ;

@class NSStringSubscription ;

@interface NSString (PubSub)

- (void) publish  ;
- (void) publish: (id) value ;
- (void) publish: (id) value userInfo: (NSDictionary *) info ;
- (id) subscribe: (void (^)(NSNotification *)) notification ;
- (NSStringSubscription *) addSubscription: (void (^)(NSNotification *)) notification ;
- (void) unsubscribe: (id) token ;

@end

@interface NSStringSubscriptionList : NSObject

@property (nonatomic, strong) NSMutableArray *  subscriptions ;

- (instancetype) addSubscription: (NSStringSubscription *) subscription ;
- (instancetype) addSubscriptionsFromArray: (NSArray *) subscriptions ;
- (instancetype) unsubscribeAll ;
//@property (NS_NONATOMIC_IOSONLY, readonly, strong) instancetype unsubscribeAll ;

@end
