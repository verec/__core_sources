//
//  NSThread+StackSymbols.m
//  UEFA
//
//  Created by verec on 02/03/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

#import "NSThread+StackSymbols.h"

@implementation NSThread (StackSymbols)

+ (NSArray *) firstFewStackSymbols: (NSInteger) howFew {
    NSArray * symbs = [NSThread callStackSymbols] ;
    NSInteger offset = 1 ;
    NSInteger total = MIN(offset+howFew, offset + [symbs count]) ;

    return [symbs subarrayWithRange:(NSRange) {
        .location   =   offset
    ,   .length     =   total
    }] ;
}

@end
