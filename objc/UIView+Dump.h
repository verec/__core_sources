//
//  UIView+Dump.h
//  __core_sources
//
//
//  Created by verec on 03/01/2015.
//  Copyright (c) 2015 Cantabilabs Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Dump)

- (void) recursiveDescription2 ;

@end
