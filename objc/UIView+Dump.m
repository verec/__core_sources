//
//  UIView+Dump.m
//  __core_sources
//
//
//  Created by verec on 03/01/2015.
//  Copyright (c) 2015 Cantabilabs Ltd. All rights reserved.
//

#import "UIView+Dump.h"

@implementation UIView (Dump)

- (void) recursiveDescription2 {
    [self recursiveDescription2: 0] ;
}

- (void) recursiveDescription2: (NSInteger) level {
    NSString * c = NSStringFromClass([self class]) ;
    NSLog(@"%*@ <%p> %@ a:%0.1f", ((int)level*2), c, self, NSStringFromCGRect(self.frame), self.alpha) ;
    if (self.superview) {
        [self.superview recursiveDescription2:1+level] ;
    }
}


@end
