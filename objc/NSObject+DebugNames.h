//
//  NSObject+DebugNames.h
//  __core_sources
//
//  Created by verec on 11/09/2015.
//  Copyright © 2015 Cantabilabs Ltd. All rights reserved.
//

@import Foundation ;

@interface NSObject (DebugNames)

- (NSString *) d ;
- (NSString *) p ;

@end
