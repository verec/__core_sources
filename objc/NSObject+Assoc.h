//
//  NSObject+Assoc.h
//  __core_sources
//
//  Created by verec on 30/10/2014.
//  Copyright (c) 2014 Cantabilabs Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Assoc)

- (NSMutableDictionary *) assoc ;

@end
