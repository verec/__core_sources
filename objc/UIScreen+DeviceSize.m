//
//  UIScreen+DeviceSize.m
//  UEFA
//
//  Created by verec on 25/02/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

#import "UIScreen+DeviceSize.h"

static CGSize deviceSizes[] = {
    {       .width  = 320.0     // ScreenSize_3_5in
    ,       .height = 480.0

    } , {   .width  = 320.0     // ScreenSize_4_0in
        ,   .height = 568.0

    } , {   .width  = 375.0     // ScreenSize_4_7in
        ,   .height = 667.0

    } , {   .width  = 414.0     // ScreenSize_5_5in
        ,   .height = 736.0

    } , {   .width  = 768.0     // ScreenSize_iPad
        ,   .height = 1024.0

    } , {   .width  = 1024.0    // ScreenSize_iPadPro
        ,   .height = 1366.0
    }
} ;

@implementation UIScreen (DeviceSize)

- (DeviceSize) deviceSize {
    CGFloat height = self.bounds.size.height ;
    CGFloat epsilon = 1.0 ;

    for (NSInteger index = DeviceSize_Count ; --index >= 0 ; ) {
        CGFloat delta = fabs(deviceSizes[index].height - height) ;
        if (delta <= epsilon) {
            return (DeviceSize) index ;
        }
    }
    

    return DeviceSize_Unknown ;
}

@end
