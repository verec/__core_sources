//
//  CustomFonts.m
//  __core_sources
//
//  Created by verec on 02/06/2013.
//  Copyright (c) 2014 Cantabilabs Ltd. All rights reserved.
//

#import "CustomFonts.h"
//#import "NSString+Utilities.h"

//#import "SwiftImport.h"

@interface NSArray (Map)
- (NSArray *) map: (id (^)(id)) map ;
- (NSArray *) filter: (BOOL (^)(id)) filter ;
@end

@implementation NSArray (Map)

- (NSArray *) map: (id (^)(id)) map {
    NSMutableArray * a = [@[] mutableCopy] ;
    for (id x in self) {
        [a addObject:map(x)] ;
    }
    return [a copy] ;
}

- (NSArray *) filter: (BOOL (^)(id)) filter {
    NSMutableArray * a = [@[] mutableCopy] ;
    for (id x in self) {
        if (filter(x)) {
            [a addObject:x] ;
        }
    }
    return [a copy] ;
}

@end

@interface NSString (Case)
- (NSArray *) componentsSeparatedByCaseChange ;
@end

@implementation NSString (Case)
- (NSArray *) componentsSeparatedByCaseChange {

    // TODO:
    // Kind of fragile, but simple. Should work for lamaCase and CamelCase
    // but will FAIL MISERABLY FOR UPPERCASE and Titlecase
    NSMutableArray * a = [@[] mutableCopy] ;

    NSString * s = @"" ;

    NSUInteger count = [self length] ;

    for (NSUInteger start = 0 ; start < count ; ++start) {

        // Ignore subtleties like grapheme clusters: we're (barely) English
        // speaking programmers, not very sophisticated ...
        unichar uniCodePoint16 = [self characterAtIndex:start] ;

        BOOL uIsUpper = isupper(uniCodePoint16) ;

        if (start) {
            if (uIsUpper) {
                if (s && [s length]) {
                    [a addObject:s] ;
                    s = @"" ;
                }
            }
        }

        s = [s stringByAppendingFormat:@"%C", uniCodePoint16] ;
    }

    if (s && [s length]) {
        [a addObject:s] ;
    }
    
    return [a copy] ;
}

@end

@implementation CustomFonts

+ (instancetype) customFonts {
    static CustomFonts * customFonts = nil ;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        customFonts = [CustomFonts new] ;
    });
    return customFonts ;
}

- (UIFont *) customFontWithName: (NSString *) name
                          style: (NSString *) style
                           size: (CGFloat) size {

    NSString * composedName = [NSString stringWithFormat:@"%@-%@", name, style] ;
    return [UIFont fontWithName:composedName size:size] ;
}

- (UIFont *) fontWithSelector: (SEL) selector
                         name: (NSString *) name
                       ofSize: (CGFloat) size {

    NSString * sel = NSStringFromSelector(selector) ;
    NSArray * comps = [sel componentsSeparatedByString:@":"] ;
    NSString * style = comps[1] ;

    NSRange ofSizeRange = [style rangeOfString:@"OfSize"] ;
    if (ofSizeRange.location != NSNotFound) {
        NSString * radix = [style substringToIndex:ofSizeRange.location] ;
        NSArray * parts = [radix componentsSeparatedByCaseChange] ;
        if (parts && [parts count]) {
            parts = [parts map:^id(NSString * comp) {
                return [comp capitalizedString] ;
            }] ;
            NSString * fontStyle= [parts componentsJoinedByString:@""] ;
            NSString * fontName = [[name capitalizedString] stringByAppendingFormat:@"-%@", fontStyle] ;

            return [UIFont fontWithName:fontName size:size] ;
        }
    }
    return nil ;
}


- (UIFont *) fontWithName: (NSString *) name blackOfSize: (CGFloat) size {
    return [self fontWithSelector:_cmd name: name ofSize:size] ;
}

- (UIFont *) fontWithName: (NSString *) name blackItalicOfSize: (CGFloat) size {
    return [self fontWithSelector:_cmd name: name ofSize:size] ;
}

- (UIFont *) fontWithName: (NSString *) name boldOfSize: (CGFloat) size {
    return [self fontWithSelector:_cmd name: name ofSize:size] ;
}

- (UIFont *) fontWithName: (NSString *) name boldItalicOfSize: (CGFloat) size {
    return [self fontWithSelector:_cmd name: name ofSize:size] ;
}

- (UIFont *) fontWithName: (NSString *) name hairlineOfSize: (CGFloat) size {
    return [self fontWithSelector:_cmd name: name ofSize:size] ;
}

- (UIFont *) fontWithName: (NSString *) name hairlineItalicOfSize: (CGFloat) size {
    return [self fontWithSelector:_cmd name: name ofSize:size] ;
}

- (UIFont *) fontWithName: (NSString *) name lightOfSize: (CGFloat) size {
    return [self fontWithSelector:_cmd name: name ofSize:size] ;
}

- (UIFont *) fontWithName: (NSString *) name lightItalicOfSize: (CGFloat) size {
    return [self fontWithSelector:_cmd name: name ofSize:size] ;
}

- (UIFont *) fontWithName: (NSString *) name italicOfSize: (CGFloat) size {
    return [self fontWithSelector:_cmd name: name ofSize:size] ;
}

- (UIFont *) fontWithName: (NSString *) name regularOfSize: (CGFloat) size {
    return [self fontWithSelector:_cmd name: name ofSize:size] ;
}

@end
