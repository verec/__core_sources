//
//  NSThread+StackSymbols.h
//  UEFA
//
//  Created by verec on 02/03/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSThread (StackSymbols)

+ (NSArray *) firstFewStackSymbols: (NSInteger) howFew ;

@end
