//
//  NSString+Localized.m
//  UEFA
//
//  Created by verec on 24/02/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

#import "NSString+Localized.h"

@implementation NSString (Localized)

- (NSString *) messageLocalized {
    return NSLocalizedString(self, @"Message") ;
}

- (NSString *) emptyLocalized {
    return NSLocalizedString(self, @"") ;
}

@end
