//
//  Graphics.swift
//  __core_sources
//
//
//  Created by verec on 29/12/2015.
//  Copyright © 2015 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

struct Graphics {

    static func decorate(view: UIView, color:UIColor = UIColor.lightGrayColor(), borderWidth:CGFloat = 0.5) {
        view.layer.borderColor = color.colorWithAlphaComponent(0.35).CGColor
        view.layer.borderWidth = borderWidth
    }

    static func undecorate(view: UIView) {
        view.layer.borderColor = UIColor.clearColor().CGColor
        view.layer.borderWidth = 0.0
    }
}