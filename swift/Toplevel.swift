//
//  Toplevel.swift
//  __core_sources
//
//  Created by verec on 25/12/2015.
//  Copyright © 2015 Cantabilabs Ltd. All rights reserved.
//

import Swift
import Foundation

import QuartzCore

func debug(items: Any..., separator: String = " ", terminator: String = "\n") {
    let payload = items.map{String($0)}
    print(payload.joinWithSeparator(separator), terminator:terminator)
//    #if DEBUG
//        if CompileTime.Flags.actuallyPrint {
//        }
//    #endif
}

func withTiming(@noescape lambda:()->()) -> NSTimeInterval {
    let start0 = CACurrentMediaTime()
    lambda()
    let end0 = CACurrentMediaTime()
    return end0 - start0
}

func scope(@noescape lamnda: () -> ()) {
    lamnda()
}