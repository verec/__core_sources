//
//  NStar.swift
//  Wordbuzz
//
//  Created by verec on 08/05/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

class NStar : UIView {

    var branchCount:        Int             = 5
    var innerWidthRatio:    Math.UnitRange  = 0.5
    var rotation:           Math.Radian?    = .None
    var fillColor:          UIColor?        = UIColor.orangeColor()
    var strokeColor:        UIColor?        = UIColor.lightGrayColor()
    var strokeWidth:        CGFloat?        = 0.5
    var inset:              CGFloat         = 16.0

    convenience init() {
        self.init(frame: CGRect.zero)
        self.backgroundColor = UIColor.clearColor()
        self.alpha = 1.0

        let reco = UITapGestureRecognizer()
        reco.addTarget(self, action: #selector(NStar.tapped(_:)))
        self.addGestureRecognizer(reco)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been, and will not be implemented.")
    }

    var onOff: Bool = false {
        didSet {
            self.setNeedsDisplay()
        }
    }

    var tapped:((NStar)->())? = .None
}

extension NStar {

    @objc func tapped(reco: UITapGestureRecognizer) {

        if reco.state == .Ended {

            GCD.MainQueue.async {
                if let tapped = self.tapped {
                    tapped(self)
                }
            }
        }
    }
}

extension NStar {

    func insetBounds() -> CGRect {
        return self.bounds
            .insetBy(dx: self.inset, dy: self.inset)

    }

    override func drawRect(rect: CGRect) {

        if !self.bounds.isDefined {
            return
        }

        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }

        let outer = insetBounds().centerAttractor().squarest(.Shortest)
        let inner = CGRect.zero.square(outer.size.width * innerWidthRatio)
                        .centered(intoRect: outer)

        let angle = Math.Constants.πx2 / CGFloat(branchCount)
        let phase = angle / 2.0

        func points(area: CGRect, phase: Math.Radian) -> [CGPoint] {

            var points = [CGPoint]()
            let center = area.center
            let radius = area.size.width / 2.0

            var running : Math.Radian = 0.0 + phase

            for _ in 0 ..< branchCount {

                let sina = sin(running)
                let cosa = cos(running)

                let point = CGPoint(x: center.x + cosa * radius
                                ,   y: center.y + sina * radius)

                points.append(point)

                running += angle
            }

            return points
        }


        let outerPoints = points(outer, phase: 0.0)
        let innerPoints = points(inner, phase: phase)

        func rotate(path: CGPath, angle: Math.Radian) -> CGPath? {
            let bounds = CGPathGetPathBoundingBox(path)
            let center = bounds.center
            var transform = CGAffineTransformIdentity
                            .translate  (center.x, center.y)
                            .rotate     (angle)
                            .translate  (center.x * -1.0, center.y * -1.0)
            return CGPathCreateCopyByTransformingPath(path, &transform)
        }

        let path = CGPathCreateMutable()
        CGPathMoveToPoint(      path, nil, outerPoints[0].x, outerPoints[0].y)
        CGPathAddLineToPoint(   path, nil, innerPoints[0].x, innerPoints[0].y)

        for px in 1 ..< outerPoints.count {
            CGPathAddLineToPoint(path, nil, outerPoints[px].x, outerPoints[px].y)
            CGPathAddLineToPoint(path, nil, innerPoints[px].x, innerPoints[px].y)
        }
        CGPathCloseSubpath(path)

        let rot:CGFloat = (rotation ?? 0.0) + Math.Constants.π + (phase / 2.0)
        let xPath = rotate(path, angle: rot) ?? path

        if let fill = fillColor {
            CGContextSetFillColorWithColor(context, fill.CGColor)
            CGContextAddPath(context, xPath)
            CGContextDrawPath(context, .Fill)
        }

        if let stroke = strokeColor {
            if let width = strokeWidth {
                CGContextSetLineWidth(context, width)
            }
            CGContextSetStrokeColorWithColor(context, stroke.CGColor)
            CGContextAddPath(context, xPath)
            CGContextDrawPath(context, .Stroke)
        }
    }
}