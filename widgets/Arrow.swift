//
//  Arrow.swift
//  __core_sources
//
//  Created by verec on 24/10/2015.
//  Copyright © 2015 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

class Arrow : UIView {

    private struct Parameters {
        static let lineSize:CGFloat                 =   3.0
        static let innerInset:CGFloat               =   lineSize
        static let outerInset:CGFloat               =   14.0
        static let flipDuration:NSTimeInterval      =   0.50
        static let restDuration:NSTimeInterval      =   0.50

        static let minAlpha:CGFloat                 =   0.30
        static let maxAlpha:CGFloat                 =   0.80
    }

    typealias TappedEast        = (arrow:Arrow, east:Bool) -> ()
    typealias TappedSouth       = (arrow:Arrow, south:Bool) -> ()

    var tappedEast:TappedEast?  = .None
    var tappedSouth:TappedSouth? = .None
    var willTapEast:[TappedEast] = []
    var willTapSouth:[TappedSouth] = []

    private var _pointsEast:Bool? = .None
    private var _pointsSouth:Bool? = .None

    var minAlpha                =   Parameters.minAlpha {
        didSet {
            animateToOtherSide()
        }
    }

    var maxAlphaLock:Bool = false

    var maxAlpha                =   Parameters.maxAlpha {
        didSet {
            animateToOtherSide()
        }
    }

    var lineSize                =   Parameters.lineSize {
        didSet {
            animateToOtherSide()
        }
    }

    var innerInset              =   Parameters.innerInset  {
        didSet {
            animateToOtherSide()
        }
    }

    var outerInset              =   Parameters.outerInset  {
        didSet {
            animateToOtherSide()
        }
    }

    var flipDuration            =   Parameters.flipDuration
    var restDuration            =   Parameters.restDuration

    var pointsEast:Bool? {
        didSet {
            _pointsEast = pointsEast
            _pointsSouth = .None
            animateToOtherSide()

            if let pointsEast = pointsEast {
                self.tappedEast?(arrow: self, east: pointsEast)
            }
        }
    }

    var pointsSouth:Bool? {
        didSet {
            if _pointsSouth == pointsSouth {
                return
            }
            _pointsSouth = pointsSouth
            _pointsEast = .None
            animateToOtherSide()

            if let pointsSouth = pointsSouth {
                self.tappedSouth?(arrow: self, south: pointsSouth)
            }
        }
    }

    func animateToOtherSide() {
        let transform : CGAffineTransform

        if let pointsEast = _pointsEast {
            transform   =   pointsEast
                        ?   CGAffineTransformIdentity
                        :   CGAffineTransformMakeScale(-1.0, 1.0)

        } else if let pointsSouth = _pointsSouth {
            transform   =   pointsSouth
                        ?   CGAffineTransformIdentity
                        :   CGAffineTransformMakeScale(1.0, -1.0)
        } else {
            transform   =   CGAffineTransformIdentity
        }

        func phaseIn() {
            self.alpha = self.maxAlpha
            self.transform = transform
        }

        func phaseOut(_:Bool = true) {
            UIView.animateWithDuration(
                self.restDuration
                ,   delay:      0.0
                ,   options:    [.AllowUserInteraction, .LayoutSubviews]
                ,   animations: {
                    if !self.maxAlphaLock && !self.activeStateIsLit {
                        self.alpha = self.minAlpha
                    }
                }
                ,   completion: nil)
        }

        self.alpha = self.minAlpha

        UIView.animateWithDuration(
            self.flipDuration
        ,   delay:      0.0
        ,   options:    [.AllowUserInteraction, .LayoutSubviews]
        ,   animations: phaseIn
        ,   completion: phaseOut)
    }

    var baseColor:UIColor = UIColor.redColor() {    /// Ugly by default
        didSet {
            self.setNeedsDisplay()
        }
    }

    var activeStateIsLit: Bool = false {
        didSet {
            GCD.MainQueue.after(0.2) {
                self.setNeedsDisplay()
            }
        }
    }

    convenience init() {
        self.init(frame: CGRect.zero)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.backgroundColor = UIColor.clearColor()
        self.alpha = self.minAlpha

        let tapper = UITapGestureRecognizer()
        self.addGestureRecognizer(tapper)
        tapper.addTarget(self, action: #selector(Arrow.tapped(_:)))

        self.pointsEast = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been, and will not be implemented.")
    }
}

extension Arrow {

    @objc func tapped(reco: UITapGestureRecognizer) {

        if reco.state == .Ended {

            if let pointsEast = _pointsEast {
                self.willTapEast.forEach {
                    $0(arrow: self, east: !pointsEast)
                }
                self._pointsSouth = .None
                self.pointsEast = !pointsEast

            } else if let pointsSouth = _pointsSouth {
                self.willTapSouth.forEach {
                    $0(arrow: self, south: !pointsSouth)
                }
                self._pointsEast = .None
                self.pointsSouth = !pointsSouth
            } else {
                self._pointsSouth = .None
                self.pointsEast = false
            }
        }
    }
}

extension Arrow {

    override func drawRect(rect: CGRect) {

        let context = UIGraphicsGetCurrentContext()

        let rect = self.bounds
            .insetBy(dx: self.innerInset, dy: self.innerInset)
            .insetBy(dx: self.outerInset, dy: self.outerInset)

        let points:[CGPoint]


        if let _ = _pointsEast {
            points = [rect.N, rect.E, rect.S]
        } else if let _ = _pointsSouth {
            points = [rect.W, rect.S, rect.E]
        } else {
            points = [rect.N, rect.W, rect.S]
        }

        let path = CGPathCreateMutable()
        for p in points {
            if CGPathIsEmpty(path) {
                CGPathMoveToPoint(path, nil, p.x, p.y)
            } else {
                CGPathAddLineToPoint(path, nil, p.x, p.y)
            }
        }

        CGContextAddPath(context, path)
        
        let color = baseColor
        CGContextSetStrokeColorWithColor(context, color.CGColor)
        CGContextSetLineWidth(context, self.lineSize)
        CGContextStrokePath(context)
    }
}