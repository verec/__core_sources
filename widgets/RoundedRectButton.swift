//
//  RoundedRectButton.swift
//  Wordbuzz
//
//  Created by verec on 11/02/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import UIKit

class RoundedRectButton: UILabel {

    var tapped: (()->())?

    convenience init() {
        self.init(frame: CGRect.zero)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.backgroundColor = UIColor.clearColor()
        self.alpha = 1.0

        self.userInteractionEnabled = true

        let tapper = UITapGestureRecognizer()
        self.addGestureRecognizer(tapper)
        tapper.addTarget(self, action: #selector(RoundedRectButton.tapped(_:)))
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been, and will not be implemented.")
    }
}

extension RoundedRectButton {

    var title: String? {
        get {
            return self.text
        }
        set {
            self.text = newValue
            self.setNeedsLayout()
        }
    }
}

extension RoundedRectButton {

    @objc func tapped(reco: UITapGestureRecognizer) {

        if reco.state == .Ended {
            self.tapped?()
        }
    }
}

extension RoundedRectButton {

    override func layoutSubviews() {
        super.layoutSubviews()

        self.layer.cornerRadius = self.bounds.size.height / 2.0
        self.layer.backgroundColor = Colors.buyButtonColor.CGColor

//        self.font = Fonts.buyButtonFont

        self.textColor = UIColor.whiteColor()
        self.textAlignment = .Center
    }
}
