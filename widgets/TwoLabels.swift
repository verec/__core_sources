//
//  TwoLabels.swift
//  __core_sources
//
//
//  Created by verec on 06/01/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

class TwoLabels : UIView {

    struct Defaults {
        static let trackColor = UIColor.greenColor().colorWithAlphaComponent(0.1)
        static let thumbColor = UIColor.greenColor().colorWithAlphaComponent(0.5)
    }

    var trackColor = Defaults.trackColor
    var thumbColor = Defaults.thumbColor

    var selectedTextColor: UIColor      = UIColor.whiteColor() {
        didSet {
            self.setNeedsLayout()
        }
    }
    var deselectedTextColor: UIColor    = UIColor.darkGrayColor() {
        didSet {
            self.setNeedsLayout()
        }
    }
    var font: UIFont                    = UIFont.systemFontOfSize(10.0) {
        didSet {
            self.setNeedsLayout()
        }
    }

    typealias TwoLabelsAction  = (Int) -> ()

    var twoLabelsAction:TwoLabelsAction?      =   .None

    let leftLabel   =   UILabel()
    let rightLabel  =   UILabel()

    var leftText:String = "Left" {
        didSet {
            leftLabel.text = leftText
            self.setNeedsLayout()
        }
    }

    var rightText:String = "Right" {
        didSet {
            rightLabel.text = rightText
            self.setNeedsLayout()
        }
    }

    var index:Int = 0 {
        didSet {
            UIView.animateWithDuration(
            0.3
            ,   delay: 0.0
            ,   options: [.LayoutSubviews, .AllowUserInteraction]
            ,   animations: {
                    self.forceLayout()
                }
            ,   completion: nil)
            
        }
    }

    convenience init() {
        self.init(frame: CGRect.zero)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.backgroundColor = UIColor.clearColor()
        self.userInteractionEnabled = true

        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(TwoLabels.tapped(_:)))

        self.addSubview(leftLabel)
        self.addSubview(rightLabel)

        setupLabel(leftLabel)
        setupLabel(rightLabel)

        self.addGestureRecognizer(tap)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been, and will not be implemented.")
    }
}

extension TwoLabels {

    func setupLabel(label: UILabel, selected:Bool = false) {
        let textColor       = selected  ?   self.selectedTextColor
                                        :   self.deselectedTextColor

        let bgColor         = selected  ?   thumbColor
                                        :   trackColor

        label.font          = self.font
        label.textColor     = textColor
        label.textAlignment = .Center
        label.numberOfLines = 1

        label.layer.backgroundColor = bgColor.CGColor
    }
}

extension TwoLabels {

    @objc func tapped(reco: UITapGestureRecognizer) {

        if reco.state == .Ended {
            let p = reco.locationInView(self)

            if leftLabel.frame.contains(p) {
                index = 1 - index
            }

            if rightLabel.frame.contains(p) {
                index = 1 - index
            }

            twoLabelsAction?(index)
        }
    }
}

extension TwoLabels {

    func layoutLabels() {

        func sizeLabel(label: UILabel) {
            label.sizeToFit()
            label.bounds = CGRect.zero.size(label.bounds.size).insetBy(dx: -10.0, dy: -2.5).home
            label.layer.cornerRadius = label.bounds.size.height / 2.0
        }

        sizeLabel(leftLabel)
        sizeLabel(rightLabel)

        setupLabel(leftLabel, selected: index == 0)
        setupLabel(rightLabel, selected: index != 0)

        let bounds = self.bounds.insetBy(dx: 40.0, dy: 0.0)

        leftLabel.frame = leftLabel.bounds.edgeAligned(toRect: bounds, edge: .Left)
        rightLabel.frame = rightLabel.bounds.edgeAligned(toRect: bounds, edge: .Right)
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        if !self.bounds.isDefined {
            return
        }

        layoutLabels()
    }
}