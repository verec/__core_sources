//
//  SimplestPieChart.swift
//  __core_sources
//
//
//  Created by verec on 31/12/2015.
//  Copyright © 2015 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

class SimplestPieChart : UIView {

    var slices:[Math.UnitRange] = [] {
        didSet {
            update()
        }
    }

    var holeSize:CGFloat        =   40.0 {
        didSet {
            update()
        }
    }

    var noDataProvider:  IndexedPalette {

        struct OneColoralette: IndexedPalette {
            func foreColor(index:Int) -> UIColor {
                return UIColor.cyanColor().colorWithAlphaComponent(0.1)
            }
        }
        return OneColoralette()
    }

    struct GaugeColorProvider : IndexedPalette {
        func foreColor(index:Int) -> UIColor {
            if index == 0 {
                return Colors.themeColor.colorWithAlphaComponent(0.2)
            }
            return Colors.themeColor.colorWithAlphaComponent(0.4)
        }
    }

    var gaugeColorProvider = GaugeColorProvider()

    var colorProvider:  IndexedPalette?

    var directions:[CGFloat]    = []
    @nonobjc var paths:[CGPath]          = []
    var colors:[UIColor]        = []

    convenience init() {
        self.init(frame: CGRect.zero)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clearColor()

        self.contentMode = .Redraw
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension SimplestPieChart {

    func update() {

        if !self.bounds.isDefined {

            /// FIXME: a bit of a hack. we're caching the path & colors to
            /// avoid unnecessary recomputing, but we need self.bounds to
            /// be valid and non empty, which is the case as soon as everything
            /// (runloop, etc ...) is settled, but not the case before the first
            /// ever draeRect is called. Hence why, in this case we keep trying
            /// 20 times per second until the bounds _is_ defined.
            /// -> This needs better thinking about chaching ... as usual with
            /// caches ...

            GCD.MainQueue.after(0.05) {
                self.update()
            }
            return
        }

        let (p, c, d) = self.precompute()

        self.paths = p
        self.colors = c
        self.directions = d

        self.setNeedsDisplay()
    }
}

extension SimplestPieChart {

    func checkSlices() -> ([Math.UnitRange], IndexedPalette, Bool) {
        if self.slices.count == 0 {
            return ([1.0], noDataProvider, true)
        } else {
            return (self.slices, colorProvider ?? noDataProvider, false)
        }
    }
}

extension SimplestPieChart {

    func precompute() -> (path:[CGPath], colors:[UIColor], directions:[CGFloat]) {

        var paths:[CGPath]          =   []
        var directions:[CGFloat]    =   []
        var colors:[UIColor]        =   []

        let (slices, colorProvider, empty) = checkSlices()

        let center              = self.bounds.squarest(.Shortest).center
        var startAngle:CGFloat  = Math.Constants.πx3_2

        for (index, value) in slices.enumerate() {

            let angle           =   value * Math.Constants.πx2
            let endAngle        =   startAngle + angle
            let midAngle        =   startAngle + (angle / 2.0)
            let radius          =   min(center.x, center.y)
            let cosa            =   cos(startAngle)
            let sina            =   sin(startAngle)
            let startPoint      =   CGPoint(x: center.x + radius * cosa
                                        ,   y: center.y + radius * sina)
            let clockWise       =   startAngle > endAngle

            let path = CGPathCreateMutable()

            CGPathMoveToPoint   (path, nil, center.x, center.y)
            CGPathAddLineToPoint(path, nil, startPoint.x, startPoint.y)
            CGPathAddArc        (path, nil, center.x, center.y
                                        ,   radius
                                        ,   startAngle,     endAngle
                                        ,   clockWise)
            CGPathCloseSubpath(path)

            startAngle = endAngle

            paths.append(path)
            colors.append(colorProvider.foreColor(index))
            if !empty {
                directions.append(midAngle)
            }
        }

        return (path:paths, colors:colors, directions:directions)
    }
}

extension SimplestPieChart {

    override func drawRect(rect: CGRect) {

        let context     = UIGraphicsGetCurrentContext()

        let holeRect    = CGRect.zero.square(holeSize)
                                    .centered(intoRect: self.bounds.squarest(.Shortest))

        CGContextSetAllowsAntialiasing      (context, true)
        CGContextSetShouldAntialias         (context, true)

        for (index, path) in self.paths.enumerate() {
            let color = self.colors[index]
            CGContextAddPath                (context, path)
            CGContextSetFillColorWithColor  (context, color.CGColor)
            CGContextDrawPath               (context, .Fill)
        }

        CGContextSetFillColorWithColor      (context, UIColor.whiteColor().CGColor)
        CGContextFillEllipseInRect          (context, holeRect)
    }
}
