//
//  FingerSlider.swift
//  __core_sources
//
//  Created by verec on 20/11/2014.
//  Copyright (c) 2014 Cantabilabs Ltd. All rights reserved.
//

import Swift

import Foundation
import UIKit


/// A thin horizontal rounded rect.

class FingerSlider : UIView, UIGestureRecognizerDelegate {

    struct Defaults {
        static let trackColor = UIColor.grayColor().colorWithAlphaComponent(0.1)
        static let thumbColor = UIColor.grayColor().colorWithAlphaComponent(0.5)
    }

    var trackColor = Defaults.trackColor
    var thumbColor = Defaults.thumbColor

    typealias FingerAction  = (CGFloat) -> ()
    typealias OnOffToggle   = (Bool) -> ()

    var gaugeOnly           =   false
    var hasTrack            =   true

    var fingerAction:FingerAction?      =   .None
    var fingerToggle:OnOffToggle?       =   .None

    #if false
    var fa:(CGFloat) -> () = MultiDispatch().call
    #endif

    var feedbackView:UIView?

    var unitValue:CGFloat = 0.5 {
        didSet {
            if isnan(unitValue) {
                unitValue = 0.0
            }
            self.setNeedsDisplay()
        }
    }

    var visible:Bool = false {
        didSet {
            if oldValue == visible {
                return
            }
            /// 0.7 ???
            UIView.animateWithDuration(oldValue ? 0.3 : 0.3) {
                self.alpha = oldValue ? 0.0 : 1.0
            }
        }
    }


    var fingerSize:CGFloat = 8.0

    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailByGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {

        if gestureRecognizer is UITapGestureRecognizer {
            if otherGestureRecognizer is UIPanGestureRecognizer {
                if otherGestureRecognizer.state.rawValue >= UIGestureRecognizerState.Began.rawValue {
                    return true
                }
            }
        }
        
        return false
    }


    convenience init() {
        self.init(frame: CGRect.zero)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.backgroundColor = UIColor.clearColor()
        self.userInteractionEnabled = true

        let pan = UIPanGestureRecognizer()
        pan.addTarget(self, action: #selector(FingerSlider.panned(_:)))
        pan.delegate = self

        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(FingerSlider.tapped(_:)))
        tap.delegate = self

        self.addGestureRecognizer(pan)
        self.addGestureRecognizer(tap)

        GCD.MainQueue.async {
            self.visible = false
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been, and will not be implemented.")
    }

    override func drawRect(rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()

        let side = self.bounds.size.width > self.bounds.size.height ? self.bounds.size.height : self.bounds.size.width

        var innerR = self.bounds
        innerR.size.width *= self.unitValue
        innerR.size.height = self.fingerSize
        innerR.origin.y = (self.bounds.size.height - self.fingerSize) / 2.0

        var outerR = innerR
        outerR.size.width = self.bounds.size.width

        let insPath : UIBezierPath
        let outPath : UIBezierPath

        if gaugeOnly {
            insPath = UIBezierPath(rect: innerR)
            outPath = UIBezierPath(rect: outerR)
        } else {
            insPath = UIBezierPath(roundedRect: innerR, cornerRadius: side / 2.0)
            outPath = UIBezierPath(roundedRect: outerR, cornerRadius: side / 2.0)
        }

        if hasTrack {
            CGContextSetFillColorWithColor(context, trackColor.CGColor)
            CGContextAddPath(context, outPath.CGPath)
            CGContextFillPath(context)
        }
        CGContextSetFillColorWithColor(context, thumbColor.CGColor)
        CGContextAddPath(context, insPath.CGPath)
        CGContextFillPath(context)
    }

    func toggle(on: Bool) {
        self.fingerToggle?(on)
    }

    func pinToBounds(p: CGPoint) -> CGPoint {
        return CGPoint( x: Math.clip(p.x, min: self.bounds.minX, max: self.bounds.maxX)
                    ,   y: Math.clip(p.y, min: self.bounds.minY, max: self.bounds.maxY))
    }

    func unitValueFromTouch(reco: UIGestureRecognizer) {
        let p = self.pinToBounds(reco.locationInView(self))
        self.unitValue = p.x / self.bounds.size.width
        self.fingerAction?(self.unitValue)
    }

    @objc func panned(reco: UIGestureRecognizer) {
        if gaugeOnly {
            return
        }
        switch reco.state {
            case .Possible: break
            case .Began:    toggle(true)
                            fallthrough
            case .Changed:  unitValueFromTouch(reco)
                            break
            case .Ended:    unitValueFromTouch(reco)
                            fallthrough
            case .Failed:   fallthrough
            case .Cancelled:toggle(false)
        }
    }

    @objc func tapped(reco: UIGestureRecognizer) {
        if gaugeOnly {
            return
        }
        switch reco.state {
            case .Ended:    fallthrough
            case .Failed:   fallthrough
            case .Cancelled:toggle(true)
                            unitValueFromTouch(reco)
                            toggle(false)
            default: break
        }
    }
}